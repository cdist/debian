# Introduction
Allows for running manifests on hosts using a docker image.

# Example
To run a simple 'ping' example, the following can be used:

## Locally
```sh
docker run --rm -it -v <path_to>/.ssh:/root/.ssh registry.gitlab.com/cdist/debian:stable /bin/sh -c "echo __ping | cdist config -v --initial-manifest - ${__target_host}"
```
Note, that if wanting to run more complex tasks rather then built-in, such as
manifests etc, also mount the .cdist directory into the container (`-v <path_to>/.cdist:/root/.cdist`) so that cdist
can find it.

Where `${__target_host}` is a host we can connect to over ssh

## .gitlab-ci.yml (or similar)
```yml
job:
  image: "registry.gitlab.com/cdist/debian:stable"
  script:
    - echo __ping | cdist config -v --initial-manifest - "<hosts>"
```
As a gitlab-runner will mount the source repository `.cdist/` does not need to
be mounted.
