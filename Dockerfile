#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: GPL-3.0+
#

FROM registry.hub.docker.com/library/debian:stable-slim AS builder

LABEL Maintainer="oliver@schinagl.nl"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# We want the latest stable version
# hadolint ignore=DL3008
RUN \
	SOURCES_LIST="$(mktemp)" sed s/deb\ /deb-src\ /g "/etc/apt/sources.list" | sort | uniq > "${SOURCES_LIST}" && \
	mv "${SOURCES_LIST}" "/etc/apt/sources.list" && \
	apt-get update && \
	apt-get build-dep --no-install-recommends -y \
		cdist \
	&& \
	rm -rf /var/cache/apt/ /var/lib/apt/ && \
	ln -s "../cdist_debian/debian" "cdist/debian" && \
	debuild -i -us -uc -b

FROM registry.hub.docker.com/library/debian:stable-slim

COPY --from=builder "cdist*.deb" "cdist.deb"

# We want the latest stable version
# hadolint ignore=DL3008
RUN \
	apt-get update && \
	apt-get install --no-install-recommends -y \
		openssh-client \
	&& \
	dpkg --install "cdist.deb" && \
	rm -rf /var/cache/apt/ /var/lib/apt/

COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
